﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceFighter
{
    /// <summary>
    /// The Enemy class 
    /// Used to initialize enemies. Controlled by the EnemyController
    /// </summary>
    public class Enemy : Sprite
    {
        private int enemyShipMovement;
        private readonly ShotController shotController;
        private double timeSinceLastShot;
        //// Ensures a delay between enemy shots
        private double ShotDelay = 1.0;

        /// <summary>
        /// Constructs the Enemy
        /// Sets the ShipSpeed (movement)
        /// </summary>
        /// <param name="texture">The graphical representation</param>
        /// <param name="position">The initial position</param>
        /// <param name="bounds">The allowed area of movement</param>
        /// <param name="shotController">A refrence to the controller calling the shots</param>
        public Enemy(Texture2D texture, Vector2 position, Rectangle bounds, ShotController shotController, int enemyShipMovement) 
            : base(texture, position, bounds)
        {
            this.shotController = shotController;
            this.enemyShipMovement = enemyShipMovement;
            ShipSpeed = enemyShipMovement;
            //increaseShootRate(); 
        }

        /*
           -- Just testing an idea about difficulty increase -- Nyvang
        private void increaseShootRate()
        { 
            ShotDelay = ShotDelay - .1;
            if (ShotDelay == 0.1)
            {
                ShotDelay = 0.9;
            }
            Console.WriteLine("ShotRate: {0}", ShotDelay);
        }
        */
        /// <summary>
        /// Generate and updates random movement and shots.
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            var random = new Random();
            //// If not moving
            if(Velocity == Vector2.Zero)
            {
                //// then move danmit!
                var direction = random.Next(2);
                Velocity = new Vector2(direction == 0 ? -1 : 1, 0);
            }
            else if (gameTime.TotalGameTime.Milliseconds % 200 <= 6) // Increase to make more rapid movement 
            {
                Console.WriteLine("EnemyDifficulty increased to: {0}", enemyShipMovement);
                if(random.Next(2) == 0)
                    Velocity = new Vector2(-Velocity.X, Velocity.Y);
            }
            //// Building the time up until it´s greater than the constant ShotDelay
            timeSinceLastShot += gameTime.ElapsedGameTime.TotalSeconds;
            if(timeSinceLastShot > ShotDelay)
            {
                //// Dont fire every time possible - have a 50/50 chance
                if(random.Next(1) == 0)
                {
                    shotController.FireEnemyShot(CalculateShotPosition());
                    timeSinceLastShot = 0;
                }   
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Calculates the position to be the hight and half the width of the <code>texture</code>
        /// Which means the center/top.
        /// The <code>position</code> ensures that the shot follows the ships position
        /// </summary>
        /// <returns>The shot position (top/center of the enemy)</returns>
        private Vector2 CalculateShotPosition()
        {
            return position + new Vector2(Width / 2, Height);
        }

        internal void RecieveHit()
        {
            IsDead = true;
        }

        public bool IsDead { get; private set; }

    }
}
