﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceFighter
{
    class Explosion : Sprite
    {
        public Explosion(Texture2D texture, Vector2 centerOfSprite, Rectangle bounds) : base(texture, centerOfSprite, bounds, 4, 2, 10)
        {
            Console.WriteLine("bounds:" + bounds);
        }

        public bool IsDone()
        {
 	        return animationPlayedOnce;
        }
    }
}
