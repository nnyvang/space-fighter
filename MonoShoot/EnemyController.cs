﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Threading;

namespace SpaceFighter
{
    /// <summary>
    /// EnemyControllers job is to create, update and draw the enemies
    /// </summary>
    public class EnemyController
    {
        private readonly Texture2D texture;
        private readonly Rectangle bounds;
        private readonly ShotController shotController;
        private int enemyShipMovement;
        private List<Enemy> enemyList = new List<Enemy>();

        /// <summary>
        /// Constructs the EnemyController
        /// </summary>
        /// <param name="texture">The graphical representation of the enemy</param>
        /// <param name="bounds">The allowed movement rectangle</param>
        /// <param name="shotController">A reference to the controller calling the shots</param>
        public EnemyController(Texture2D texture, Rectangle bounds, ShotController shotController)
        {
            this.texture = texture;
            this.bounds = bounds;
            this.shotController = shotController;
            enemyShipMovement = 200;

            CreateEnemy(enemyShipMovement);
        }

        /// <summary>
        /// Created an <code>enemy</code> and adds the object to <code>enemyList</code> 
        /// </summary>
        private void CreateEnemy(int enemyDifficulty)
        {
            var position = RandomPosition();
            var enemy = new Enemy(texture, position, bounds, shotController, enemyShipMovement);
            enemyList.Add(enemy);
        }

        /// <summary>
        /// Returns an IEnumerable list
        /// from <code>enemyList</code>
        /// </summary>
        public IEnumerable<Enemy> Enemies
        {
            get { return enemyList; }
        }

        /// <summary>
        /// Draws contents of the <code>enemyList</code> 
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var enemy in enemyList)
                enemy.Draw(spriteBatch); 
        }
        
        /// <summary>
        /// Creates a random position for the enemy to spawn
        /// Note: The Y-position is fixed, i.e. the enemy only moves left & right
        /// </summary>
        /// <returns>A random Vector2 position</returns>
        private Vector2 RandomPosition()
        {
            var random = new Random();
            var positionX = random.Next(bounds.Width - texture.Width + 1);
            var positionY = 20.0f;
            return new Vector2(positionX, positionY);
        }

        /// <summary>
        /// Updates contents 
        /// Runns through <code>enemyList</code> and checks if anyone
        /// have the boolean <code>IsDead</code> field set to <code>true</code>
        /// If an enemy is dead, a new one is created
        /// 
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < enemyList.Count(); i++)
            {
                if (enemyList[i].IsDead)
                {
                    enemyList.Remove(enemyList[i]);
                    enemyShipMovement += 3;
                    CreateEnemy(enemyShipMovement);      
                }
                enemyList[i].Update(gameTime);
            }
        }

        /// <summary>
        /// Counts the number of enemies where
        /// the boolean <code>IsDead</code> field set to <code>true</code>
        /// </summary>
        /// <returns>Number (int) of dead enemies</returns>
        public int getKillCount()
        {
            return Enemies.Where(e => e.IsDead).Count();
        }
    }
}
