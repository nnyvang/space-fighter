﻿// -----------------------------------------------------------------------
// <copyright file="CollisionController.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SpaceFighter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Xna.Framework;

    /// <summary>
    /// Checks for collisions and alerts the different gameObjects involved.
    /// </summary>
    public class CollisionController
    {
        private readonly PlayerShip playerShip;
        private readonly EnemyController enemyController;
        private readonly ShotController shotController;
        private readonly ExplosionController explosionController;

        public CollisionController(PlayerShip playerShip, EnemyController enemyController, ShotController shotController, ExplosionController explosionController)
        {
            this.playerShip = playerShip;
            this.enemyController = enemyController;
            this.shotController = shotController;
            this.explosionController = explosionController;

        }

        public void Update(GameTime gameTime)
        {
            CheckForCollitions();
        }

        private void CheckForCollitions()
        {
            CheckShotAtPlayer();
            CheckShotFromEnemy();
        }

        private void CheckShotFromEnemy()
        {
            foreach (var shot in shotController.EnemyShot)
            {
                if (shot.BoundingBox.Intersects(playerShip.BoundingBox))
                    playerShip.RecieveHit();
            }
        }

        private void CheckShotAtPlayer()
        {
            for (int i = 0; i < shotController.PlayerShot.Count(); i++)
            {
                var shot = shotController.PlayerShot[i];

                    foreach (var enemy in enemyController.Enemies)
                    {   // If the enemy is alive i.e. not dead... and is hit by shot
                        if (!enemy.IsDead && shot.BoundingBox.Intersects(enemy.BoundingBox))
                        {
                            enemy.RecieveHit();
                            if (enemy.IsDead)
                                explosionController.CreateExplosion(enemy);
                            shotController.RemovePlayerShot(shot);
                        }
                    }
                
            }
        }
    }
}
