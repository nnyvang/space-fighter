﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SpaceFighter
{
    /// <summary>
    /// 
    /// </summary>
    public class Sprite
    {
        private readonly Texture2D texture;
        public Vector2 position;
        private readonly Rectangle movementBounds;
        private readonly int rows;
        private readonly int columns;
        private readonly double framesPerSecond;
        private int totalFrames;
        private double timeSinceLastFrame;
        private int currentFrame;
        protected bool animationPlayedOnce;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="position"></param>
        /// <param name="movementBounds"></param>
        public Sprite(Texture2D texture, Vector2 position, Rectangle movementBounds) : this(texture, position, movementBounds, 1, 1, 1)
        {

        }

        public Sprite(Texture2D texture, Vector2 position, Rectangle movementBounds, int rows, int columns, double framesPerSecond)
        {
            this.texture = texture;
            this.position = position;
            this.movementBounds = movementBounds;
            this.rows = rows;
            this.columns = columns;
            this.framesPerSecond = framesPerSecond;
            totalFrames = rows * columns;

        }

        /// <summary>
        /// Finding the current frame from the texture
        /// and draws it...
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            var imageWidth = texture.Width / columns;
            var imageHeight = texture.Height / rows;

            var currentRow = currentFrame / columns;
            var currentColumn = currentFrame % columns;

            var sourceRectangle = new Rectangle(imageWidth * currentColumn, imageHeight * currentRow, imageWidth, imageHeight);

            var destinationRectangle = new Rectangle((int)position.X, (int)position.Y, imageWidth, imageHeight);
            spriteBatch.Draw(texture, destinationRectangle, sourceRectangle, Color.White);
        }

       /// <summary>
       /// Setting the new position based on the velocity and the time between update-calls.
       /// 
       /// First we create a test position to check wheter the new position is out of bounds
       /// if this is the case we return nothing, that is we do nothing. Otherwise, of course, 
       /// we update the position
       /// </summary>
       /// <param name="keyBoardState"></param>
       /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
        {
            UpdateAnimation(gameTime);

           var newPosition = position + ((Velocity * (float)gameTime.ElapsedGameTime.TotalSeconds) * ShipSpeed);
            if (Blocked(newPosition))
            {
                return;
            }

            position = newPosition; 
        }

        private void UpdateAnimation(GameTime gameTime)
        {
            timeSinceLastFrame += gameTime.ElapsedGameTime.TotalSeconds;
            if (timeSinceLastFrame > SecondsBetweenFrames())
            {
                currentFrame++;
                timeSinceLastFrame = 0;
            }

            if (currentFrame == totalFrames)
            {
                currentFrame = 0;
                animationPlayedOnce = true;
            }
        }

        public double SecondsBetweenFrames() 
        {
            return 1 / framesPerSecond;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newPosition"></param>
        /// <returns></returns>
        private bool Blocked(Vector2 newPosition)
        {
            var boundingBox = CreateBoundingBoxFromPosition(newPosition);
            return !movementBounds.Contains(boundingBox);
                 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        private Rectangle CreateBoundingBoxFromPosition(Vector2 position)
        {
            //// Thats around 4 too manu type casts according to my old teacher - dont tell him...
            return new Rectangle((int) position.X, (int) position.Y, (int) Width, (int) Height);
        }

        /// <summary>
        /// 
        /// </summary>
        public Rectangle BoundingBox
        {
            get { return CreateBoundingBoxFromPosition(position); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector2 Velocity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float Width { get { return texture.Width/columns; } }

        /// <summary>
        /// 
        /// </summary>
        public float Height { get { return texture.Height/rows; } }

        /// <summary>
        /// ShipSpeed is a constant that ensures the same fly speed at all times.
        /// </summary>
        protected float ShipSpeed { get; set; }


        
    }
}
