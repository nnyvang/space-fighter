﻿// -----------------------------------------------------------------------
// <copyright file="PlayerShip.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SpaceFighter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Input;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PlayerShip : Sprite
    {
        private readonly ShotController shotController;

        private const double TimeBetweenShotsInSeconds = 1;
        private double timeSinceLastShotInSeconds = 0;

        public PlayerShip(Texture2D texture, Vector2 position, Rectangle movementBounds, ShotController shotController) 
            : base(texture, position, movementBounds, 4, 2, 20)
        {
            this.shotController = shotController;
            ShipSpeed = 300;
            IsDead = false;
        }

        public override void Update(GameTime gameTime) 
        {
            timeSinceLastShotInSeconds += gameTime.ElapsedGameTime.TotalSeconds;
            UpdateVelocity();
            base.Update(gameTime);
        }

        /**
        * Handles controls using the keyboard state. 
        * Usable keys:
        *  - Left Arrow
        *  - Right Arrow
        *  - Up Arrow
        *  - Down Arrow
        *  
        *  ShipSpeed is used to maintain a constant flying speed.
        *  Can be changed if needed in a later version.
        */
        private void UpdateVelocity()
        {
            var keyboardState = Keyboard.GetState();
            UpdateVelocityFromKeyboard(keyboardState);
            CheckForShotFromKeyboard(keyboardState);
        }

        private void CheckForShotFromKeyboard(KeyboardState keyboardState)
        {
            if (keyboardState.IsKeyDown(Keys.Space) && ShotAllowed())
            {
                shotController.FirePlayerShot(CalculateShotPosition());
                timeSinceLastShotInSeconds = 0;
            }
        }

        private bool ShotAllowed()
        {
            return (timeSinceLastShotInSeconds > TimeBetweenShotsInSeconds) && !IsDead;
            
        }
        /// <summary>
        /// Calculates the position to be the hight and half the width of the <code>texture</code>
        /// Which means the center/top.
        /// The <code>position</code> ensures that the shot follows the ships position
        /// </summary>
        /// <returns>The shot position (top/center of the player)</returns>
        private Vector2 CalculateShotPosition()
        {
            return position + new Vector2(Width / 2, 0);
        }

        private void UpdateVelocityFromKeyboard(KeyboardState keyboardState)
        {
            var keyDictionary = new Dictionary<Keys, Vector2>
                                    {
                                        {Keys.Left, new Vector2(-1, 0)},
                                        {Keys.Right, new Vector2(1, 0)},
                                        {Keys.Up, new Vector2(0, -1)},
                                        {Keys.Down, new Vector2(0, 1)},
                                    };

            var velocity = Vector2.Zero;
            foreach (var keypress in keyboardState.GetPressedKeys())
            {
                try
                {
                    velocity += keyDictionary[keypress];
                }
                catch (KeyNotFoundException)
                {
                    //// Try Catch is needed because an exception is thrown when a key, different from the ones in keyDictionary
                    //// IGNORE
                }
            }

            Velocity = velocity;

        }

        private void Checkounds()
        {
            
        }

        internal void RecieveHit()
        {
            IsDead = true;
        }

        public bool IsDead { get; private set; }
    }
}
