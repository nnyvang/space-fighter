﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceFighter
{
    /// <summary>
    /// The shot class...
    /// </summary>
    public class Shot : Sprite
    {
        /// <summary>
        /// Constructs the shot itself and setting the shot speed.
        /// </summary>
        /// <param name="texture">The graphical image</param>
        /// <param name="position">The startposition of the shot</param>
        /// <param name="movementBounds">The area of the screen where movement is allowed</param>
        public Shot(Texture2D texture, Vector2 position, Rectangle movementBounds)
            : base(texture, position, movementBounds)
        {
            ShipSpeed = 200;
        }
    }
}
