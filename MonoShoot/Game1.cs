﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
#endregion

namespace SpaceFighter
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Sprite background;
        private PlayerShip playerShip;
        private SpriteFont gameFont;
        private EnemyController enemyController;
        private ShotController shotController;
        private CollisionController collisionContoller;
        private ExplosionController explosionController;
        private SoundController soundController; 
        

        private int score = 0;

        private readonly int gameDimensionHeight = 800;
        private readonly int gameDimensionWidth = 600;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = gameDimensionHeight;
            graphics.PreferredBackBufferWidth = gameDimensionWidth;
            Content.RootDirectory = "Content";
        }
        /// <summary>
        /// Initialize the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content. Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            background = new Sprite(Content.Load<Texture2D>("space_bg_600x800"), Vector2.Zero, graphics.GraphicsDevice.Viewport.Bounds);

            // Load the background sound 
            //backgroundSound = Content.Load<SoundEffect>("ambience1");
            soundController = new SoundController(Content);

            var shipTexture = Content.Load<Texture2D>("chromeAnim");
            var shipPositionX = (graphics.GraphicsDevice.Viewport.Width / 2) - (shipTexture.Width / 2);
            var shipPositionY = graphics.GraphicsDevice.Viewport.Height - shipTexture.Height - 10;
            var playerBounds = new Rectangle(0, graphics.GraphicsDevice.Viewport.Height - 600, graphics.GraphicsDevice.Viewport.Width, 600);

            shotController = new ShotController(Content.Load<Texture2D>("chromeShot"), Content.Load<Texture2D>("ieShot"), graphics.GraphicsDevice.Viewport.Bounds, soundController);
            playerShip = new PlayerShip(shipTexture, new Vector2(shipPositionX, shipPositionY), playerBounds, shotController);

            enemyController = new EnemyController(Content.Load<Texture2D>("ieEnemy"), graphics.GraphicsDevice.Viewport.Bounds, shotController);
            explosionController = new ExplosionController(Content.Load<Texture2D>("explosionAnim"), graphics.GraphicsDevice.Viewport.Bounds, soundController);
            collisionContoller = new CollisionController(playerShip, enemyController, shotController, explosionController);

            gameFont = Content.Load<SpriteFont>("GameFont");

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            
            playerShip.Update(gameTime);
            enemyController.Update(gameTime);
            shotController.Update(gameTime);
            explosionController.Update(gameTime);
            collisionContoller.Update(gameTime);
            UpdateScore();
            base.Update(gameTime);
        }

        private void UpdateScore()
        {
            var kills = enemyController.getKillCount();
            score += (kills * 1000);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            //// Throw the background at the screen - first one, so it is placed at the bottom
            background.Draw(spriteBatch);
            //// Insert the playerShip next to make it appear on top if its not dead
            if(!playerShip.IsDead)
                playerShip.Draw(spriteBatch);
            /// Draw the emenies contanied in the EnemyController
            enemyController.Draw(spriteBatch);
            shotController.Draw(spriteBatch);
            explosionController.Draw(spriteBatch);
            /* Drawing the Score */
            //// Format the string to fit our needs
            var scoreText = string.Format("Score: {0}", score);
            //// Using the MeasureString method we get the dimensions of the string in order to place it correctly on the X axis
            var scoreDimensions = gameFont.MeasureString(scoreText);
            //// Getting data for the placement of the score text - used in the new Vector2 upon drawig
            var scoreX = graphics.GraphicsDevice.Viewport.Width - scoreDimensions.X - 2;
            var scoreY = 0 + scoreDimensions.Y - 58;
            
            //// Draw the score to the game screen 
            spriteBatch.DrawString(gameFont, scoreText, new Vector2(scoreX, scoreY), Color.White);

            //// After complete draw, we need to close the spriteBatch and call super´s Draw Method
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
