using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace SpaceFighter
{
    /// <summary>
    /// The SoundController class handles (load n� play) the sounds of the game. 
    /// </summary>
    public class SoundController
    {
        private SoundEffectInstance backgroundEffect;
        private SoundEffectInstance soundEffect;
        private SoundEffect backgroundAmbient;
        private SoundEffect laserEffect;
        private SoundEffect explosionEffect;

        /// <summary>
        /// Creates an insance of the SoundController and loads the different audio files
        /// </summary>
        /// <param name="content">The content manager from where the audio files are loaded</param>
        public SoundController(ContentManager content)
        {
            backgroundAmbient = content.Load<SoundEffect>("ambience1");
            laserEffect = content.Load<SoundEffect>("laserblast");
            explosionEffect = content.Load<SoundEffect>("explosion_short");
            loopBackgroundSound(backgroundAmbient);
        }

        /// <summary>
        ///  PLays and loops the background sound
        /// </summary>
        /// <param name="soundEffect"></param>
        public void loopBackgroundSound(SoundEffect soundEffect)
        {
            backgroundEffect = soundEffect.CreateInstance();
            backgroundEffect.IsLooped = true;
            backgroundEffect.Volume = 0.1f;
            backgroundEffect.Play();
        }

        /// <summary>
        /// Creates an soundEffect of a given soundeffect which is configured in the parameters.
        /// Note, that the default volume for e.g. a shot is 0.2f 
        /// </summary>
        /// <param name="soundToPlay">The soundEffect which shoud be played</param>
        /// <param name="volume">Indicates the given volume for the soundEffect</param>
        public void playGeneralSound(SoundEffect soundToPlay, float volume, bool isLooped)
        {
            soundEffect = soundToPlay.CreateInstance();
            soundEffect.Volume = volume;
            soundEffect.IsLooped = isLooped;
            soundEffect.Play();
        }

        /// <summary>
        /// Plays the lasereffect, which is currently used when shots are fired.
        /// </summary>
        public void playLaserEffect()
        {
            playGeneralSound(laserEffect, 0.1f, false);
        }

        /// <summary>
        /// Plays the explosion with a fixed volume, and un-looped.
        /// </summary>
        public void playExplosion()
        {

            playGeneralSound(explosionEffect, 0.1f, false);
        }

    }
}
