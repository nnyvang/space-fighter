﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceFighter
{
    public class ExplosionController
    {
        private Texture2D texture;
        private Rectangle bounds;
        private List<Explosion> explosions = new List<Explosion>();
        private SoundController soundController;

        public ExplosionController(Texture2D texture, Rectangle bounds, SoundController soundController)
        {
            this.texture = texture;
            this.bounds = bounds;
            this.soundController = soundController;
        }

        public void CreateExplosion(Sprite sprite)
        {
            var centerOfSprite = new Vector2(sprite.position.X + (sprite.Width / 2), sprite.position.Y + (sprite.Height / 2));
            var explosion = new Explosion(texture, centerOfSprite, bounds);
            explosion.Position -= new Vector2(explosion.Width /2, explosion.Height /2);
            explosions.Add(explosion); 
            soundController.playExplosion();
        }

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < explosions.Count; i++)
            {
                explosions[i].Update(gameTime);
                if (explosions[i].IsDone())
                    explosions.Remove(explosions[i]);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var explosion in explosions)
            {
                explosion.Draw(spriteBatch);
            }
        }
    }
}
