﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceFighter
{
    /// <summary>
    /// ShotController is managing the shots that are requested by 
    /// either the enemy or the player.
    /// </summary>
    public class ShotController
    {
        private readonly Texture2D playerTexture, enemyTexture;
        private readonly Rectangle bounds;
        private List<Shot> enemyShots = new List<Shot>();
        private List<Shot> playerShots = new List<Shot>();
        private SoundController soundController;

        /// <summary>
        /// Creates a union of the two lists
        /// It doesnt need to be changes so it is IEnumerable
        /// </summary>
        private IEnumerable<Shot> AllShots
        {
            get { return enemyShots.Union(playerShots); } // 
        }

        /// <summary>
        /// Constructs a ShotController
        /// </summary>
        /// <param name="texture">The graphical 2D image</param>
        /// <param name="bounds">The bounds of the game</param>
        public ShotController(Texture2D playerTexture, Texture2D enemyTexture, Rectangle bounds, SoundController soundController)
        {
            this.playerTexture = playerTexture;
            this.enemyTexture = enemyTexture;
            this.bounds = bounds;
            this.soundController = soundController;
        }

        /// <summary>
        /// Creates a new shot and adds it to a list of enemy shots
        /// thats queueing up for fireing
        /// </summary>
        /// <param name="shotPosition">Startposition of the shot</param>
        public void FireEnemyShot(Vector2 shotPosition)
        {
            FireShot(enemyTexture,shotPosition, 1, enemyShots);
        }

        /// <summary>
        /// Updates the shots in <c>enemyShots</c>
        /// To remove shots when they go off screen (their bounds are viewport + 15px )
        /// we are checking if the bounds doesnt contain the enemyShot with the current index
        /// If so, we remove it - and it wont get drawn in the next cycle.
        /// 
        /// Actually there´s no visual needs for removing the shots because the extra bounds
        /// mentioned above is bigger than the shotTexture height, which means that we cannot see them
        /// anyways. But eventually theres alot of shots and it can cost quite a amount of memory..
        /// And mabye the shot texture will be changed in the future
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public void Update(GameTime gameTime)
        {
            foreach (var shot in AllShots)
                shot.Update(gameTime);

            for (int i = 0; i < enemyShots.Count; i++)
            {
                if (!bounds.Contains(enemyShots[i].BoundingBox))
                    enemyShots.Remove(enemyShots[i]);
            }

            for (int i = 0; i < playerShots.Count; i++)
            {
                if (!bounds.Contains(playerShots[i].BoundingBox))
                    playerShots.Remove(playerShots[i]);
            }
        }

        /// <summary>
        /// Draws any updates made to the shots in <c>enemyShots</c>
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var shot in AllShots)
            {
                shot.Draw(spriteBatch);
            }
        }

        internal IEnumerable<Shot> EnemyShot
        {
            get { return enemyShots; }
        }

        internal IList<Shot> PlayerShot
        {
            get { return playerShots; }
        }

        /// <summary>
        /// Create shots based on either the player or the enemey.
        /// Adds an inflated bound to make sure that the shots will go off screen
        /// Setting the shot direction based on the enemy or player.
        /// Adds the created shot to a list
        /// </summary>
        /// <param name="texture">The enemy/player shot image</param>
        /// <param name="shotPosition">The ships position where the shot spawns</param>
        /// <param name="yDirection"></param>
        /// <param name="shotList"></param>
        private void FireShot(Texture2D texture, Vector2 shotPosition, int yDirection, List<Shot> shotList)
        {
            var inflatedBounds = bounds;
            inflatedBounds.Inflate(15, 15);
            var shot = new Shot(texture, shotPosition, inflatedBounds);
            shot.Velocity = new Vector2(0, yDirection);
            shotList.Add(shot);
        }

        public void FirePlayerShot(Vector2 shotPosition)
        {
            FireShot(playerTexture, shotPosition, -1, playerShots);
            soundController.playLaserEffect();
        }

        public void RemovePlayerShot(Shot shot)
        {
            playerShots.Remove(shot);
        }
    }
}
